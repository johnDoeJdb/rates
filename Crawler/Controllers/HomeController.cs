﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;

namespace Crawler.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public string Index()
        {
            string type = Request.QueryString["exchtype"];
            string line;
            string wmUrl = "https://wmeng.exchanger.ru/asp/XMLWMList.asp?exchtype="+type;
            WebRequest webRequest = WebRequest.Create(wmUrl);
            WebResponse webResponse = webRequest.GetResponse();
            using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
            {
                line = sr.ReadLine();
            }

            return line;
        }

    }
}
